package com.example.flow

import co.paralleluniverse.fibers.Suspendable
import net.corda.core.contracts.requireThat
import net.corda.core.flows.*
import net.corda.core.identity.Party
import net.corda.core.transactions.SignedTransaction
import net.corda.core.transactions.TransactionBuilder
import net.corda.core.utilities.ProgressTracker
import net.corda.core.utilities.ProgressTracker.Step


object SettleFlow {
    @InitiatingFlow
    @StartableByRPC
    class Initiator(val iouValue: Int,
                    val otherParty: Party) : FlowLogic<SignedTransaction>() {
        /**
         * The progress tracker checkpoints each stage of the flow and outputs the specified messages when each
         * checkpoint is reached in the code. See the 'progressTracker.currentStep' expressions within the call() function.
         */
        companion object {
            object GENERATING_TRANSACTION : Step("Generating transaction based on new IOU.")
            object VERIFYING_TRANSACTION : Step("Verifying contract constraints.")
            object SIGNING_TRANSACTION : Step("Signing transaction with our private key.")
            object GATHERING_SIGS : Step("Gathering the counterparty's signature.") {
                override fun childProgressTracker() = CollectSignaturesFlow.tracker()
            }

            object FINALISING_TRANSACTION : Step("Obtaining notary signature and recording transaction.") {
                override fun childProgressTracker() = FinalityFlow.tracker()
            }

            fun tracker() = ProgressTracker(
                    GENERATING_TRANSACTION,
                    VERIFYING_TRANSACTION,
                    SIGNING_TRANSACTION,
                    GATHERING_SIGS,
                    FINALISING_TRANSACTION
            )
        }

        override val progressTracker = tracker()

        /**
         * The flow logic is encapsulated within the call() method.
         */
        @Suspendable
        override fun call(): SignedTransaction {

            /*###########################################################
            ###### TODO 1.VaultQuery (more info https://docs.corda.net/api-vault-query.html)
            ###########################################################*/
//            val queryCriteria =
//            val inputState =

            /*###########################################################
            ###### TODO 2.Select Notary from inputState
            ###########################################################*/

//            val notary =


            /*###########################################################
            ###### TODO 3.Build transaction
            ###########################################################*/
            progressTracker.currentStep = GENERATING_TRANSACTION

//            val iouState =
//            val txCommand =
//            val txBuilder =

            /*###########################################################
            ###### TODO 4.Verify transaction
            ###########################################################*/
            progressTracker.currentStep = VERIFYING_TRANSACTION
//            txBuilder.?

            /*###########################################################
            ###### TODO 5.Initial sign transaction
            ###########################################################*/
            progressTracker.currentStep = SIGNING_TRANSACTION
//            val partSignedTx =

            /*###########################################################
            ###### TODO 6.Collect signature from other node
            ###########################################################*/
            progressTracker.currentStep = GATHERING_SIGS
//            val otherPartySession =
//            val fullySignedTx =

            /*###########################################################
            ###### TODO 7.Finalise flow and commit transaction to vault of each node
            ###########################################################*/
            progressTracker.currentStep = FINALISING_TRANSACTION
            // Notarise and record the transaction in both parties' vaults.
            //(This line is wrong, don't trust me!!!)
            return subFlow(CollectSignaturesFlow(serviceHub.signInitialTransaction(TransactionBuilder(otherParty)), setOf(initiateFlow(otherParty)), GATHERING_SIGS.childProgressTracker()))
        }
    }

    @InitiatedBy(Initiator::class)
    class Responder(val otherPartySession: FlowSession) : FlowLogic<SignedTransaction>() {
        @Suspendable
        override fun call(): SignedTransaction {
            val signTransactionFlow = object : SignTransactionFlow(otherPartySession) {
                override fun checkTransaction(stx: SignedTransaction) = requireThat {
                    //You can ignore it (just for this bootcamp) :)
                }
            }
            val txId = subFlow(signTransactionFlow).id

            return subFlow(ReceiveFinalityFlow(otherPartySession, expectedTxId = txId))
        }
    }
}
